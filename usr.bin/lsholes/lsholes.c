/*
 * Copyright (c) 2016 Maxim Sobolev <sobomax@FreeBSD.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 */

#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>
#include <assert.h>
#include <inttypes.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

static void usage(void);

#define D_TO_PERC(x) ((x) * (double)100.0)

static int
nddigits(uintmax_t n)
{
    int nd;

    for (nd = 1; n > 9; n /= 10) {
        nd++;
    }
    return (nd);
}

int
main(int argc, char **argv)
{
    int vflag, ch, fd, hflag, dflag;
    off_t cur, hole, data, firsthole, firstdata;
    size_t holesize, datasize, holetotal, datatotal;
    struct stat sbs;
    double hperc, dperc;
    int data_width;
    char *print_format, *header_format;

    hflag = dflag = 0;
    vflag = 0;
    while ((ch = getopt(argc, argv, "vhd")) != -1) {
        switch (ch) {
        case 'v':
            vflag = 1;
            break;
        case 'h':
            hflag = 1;
            break;
        case 'd':
            dflag = 1;
            break;
        case '?':
        default:
            usage();
        }
    }
    argc -= optind;
    argv += optind;
    if (argc != 1) {
        fprintf(stderr, "lsholes: requires an argument\n");
        usage();
    }
    if (hflag != 0 && dflag != 0) {
        fprintf(stderr, "lsholes: -d and -h flags are mutually exclusive\n");
        usage();
    }
    fd = open( argv[0], O_RDONLY, 0);
    if (fd < 0) {
        fprintf(stderr, "lsholes: %s: %s\n", argv[0], strerror(errno));
        exit(1);
    }
    if (fstat(fd, &sbs) < 0) {
        fprintf(stderr, "lsholes: %s: %s\n", argv[0], strerror(errno));
        exit(1);
    }
    data_width = nddigits(sbs.st_size);
    if (data_width < 5) {
        data_width = 5;
    }
    asprintf(&header_format, "%s%d%s%d%s%d%s", "%s %", data_width, "s %",
      data_width, "s %", data_width, "s\n");
    if (header_format == NULL) {
        fprintf(stderr, "lsholes: %s: %s\n", argv[0], strerror(ENOMEM));
        exit(1);
    }
    asprintf(&print_format, "%s%d%s%d%s%d%s", "%s %", data_width,
      "jd %", data_width, "jd %", data_width, "jd\n");
    if (print_format == NULL) {
        fprintf(stderr, "lsholes: %s: %s\n", argv[0], strerror(ENOMEM));
        exit(1);
    }
#if 0
    printf("%d %s %d %d\n", argc, argv[0], fd, nddigits(sbs.st_size));
#endif
    firsthole = lseek(fd, 0, SEEK_HOLE);
    firstdata = lseek(fd, 0, SEEK_DATA);
    if (sbs.st_size > 0 && firsthole < 0 && firstdata < 0) {
        fprintf(stderr, "lsholes: %s: %s\n", argv[0], strerror(errno));
        exit(1);
    }
    printf(header_format, "Type", "Start", "End", "Size");
    holetotal = datatotal = 0;
    for (cur = 0; cur < sbs.st_size; cur = data) {
        hole = lseek(fd, cur, SEEK_HOLE);
        if (hole < 0) {
            fprintf(stderr, "lsholes: %s: %s\n", argv[0], strerror(errno));
            exit(1);
        }
        if (hole > cur) {
            datasize = hole - cur;
            datatotal += datasize;
            if (hflag == 0) {
                printf(print_format, "DATA", (intmax_t)cur, (intmax_t)hole - 1,
                  (intmax_t)datasize);
            }
            if (hole >= sbs.st_size) {
              break;
            }
        }
        data = lseek(fd, hole, SEEK_DATA);
        if (data < 0 && errno != ENXIO) {
            fprintf(stderr, "lsholes: %s: %s\n", argv[0], strerror(errno));
            exit(1);
        }
        if (data > 0) {
            holesize = data - hole;
        } else {
            holesize = sbs.st_size - hole;
            data = sbs.st_size;
        }
        holetotal += holesize;
        if (dflag == 0) {
            printf(print_format, "HOLE", (intmax_t)hole, (intmax_t)data - 1,
              (intmax_t)holesize);
        }
    }
    assert(holetotal + datatotal == (size_t)sbs.st_size);
    if (sbs.st_size > 0) {
        hperc = (double)(holetotal) / (double)(sbs.st_size);
        dperc = (double)1.0 - hperc;
    } else {
        hperc = dperc = 0.0;
    }
    printf("\n");
    if (dflag == 0) {
        printf("Total HOLE: %jd (%.2f%%)\n", (intmax_t)holetotal,
          D_TO_PERC(hperc));
    }
    if (hflag == 0) {
        printf("Total DATA: %jd (%.2f%%)\n", (intmax_t)datatotal,
          D_TO_PERC(dperc));
    }
    close(fd);
    exit(0);
}

static void
usage(void)
{

    fprintf(stderr, "usage: lsholes [-v] [-h|d] file\n");
    exit(1);
}
