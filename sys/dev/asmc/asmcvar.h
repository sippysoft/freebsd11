/*-
 * Copyright (c) 2007, 2008 Rui Paulo <rpaulo@FreeBSD.org>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * $FreeBSD: head/sys/dev/asmc/asmcvar.h 271975 2014-09-22 16:20:38Z rpaulo $
 *
 */

#define ASMC_MAXFANS	6

struct asmc_softc {
	device_t 		sc_dev;
	struct mtx 		sc_mtx;
	int 			sc_nfan;
	int16_t			sms_rest_x;
	int16_t			sms_rest_y;
	int16_t			sms_rest_z;
	struct sysctl_oid 	*sc_fan_tree[ASMC_MAXFANS+1];
	struct sysctl_oid 	*sc_temp_tree;
	struct sysctl_oid 	*sc_sms_tree;
	struct sysctl_oid 	*sc_light_tree;
	struct asmc_model 	*sc_model;
	int 			sc_rid_port;
	int 			sc_rid_irq;
	struct resource 	*sc_ioport;
	struct resource 	*sc_irq;
	void 			*sc_cookie;
	int 			sc_sms_intrtype;
	struct taskqueue 	*sc_sms_tq;
	struct task 		sc_sms_task;
	uint8_t			sc_sms_intr_works;
};

/*
 * Data port.
 */
#define ASMC_DATAPORT_READ(sc)	bus_read_1(sc->sc_ioport, 0x00)
#define ASMC_DATAPORT_WRITE(sc, val) \
	bus_write_1(sc->sc_ioport, 0x00, val)
#define ASMC_STATUS_MASK 	0x0f

/*
 * Command port.
 */
#define ASMC_CMDPORT_READ(sc)	bus_read_1(sc->sc_ioport, 0x04)
#define ASMC_CMDPORT_WRITE(sc, val) \
	bus_write_1(sc->sc_ioport, 0x04, val)
#define ASMC_CMDREAD		0x10
#define ASMC_CMDWRITE		0x11

/*
 * Interrupt port.
 */
#define ASMC_INTPORT_READ(sc)	bus_read_1(sc->sc_ioport, 0x1f)


/* Number of keys */
#define ASMC_NKEYS		"#KEY"	/* RO; 4 bytes */ 

/*
 * Fan control via SMC.
 */
#define ASMC_KEY_FANCOUNT	"FNum"	/* RO; 1 byte */
#define ASMC_KEY_FANMANUAL	"FS! "	/* RW; 2 bytes */
#define ASMC_KEY_FANID		"F%dID"	/* RO; 16 bytes */
#define ASMC_KEY_FANSPEED	"F%dAc"	/* RO; 2 bytes */
#define ASMC_KEY_FANMINSPEED	"F%dMn"	/* RO; 2 bytes */
#define ASMC_KEY_FANMAXSPEED	"F%dMx"	/* RO; 2 bytes */
#define ASMC_KEY_FANSAFESPEED	"F%dSf"	/* RO; 2 bytes */
#define ASMC_KEY_FANTARGETSPEED	"F%dTg"	/* RW; 2 bytes */

/*
 * Sudden Motion Sensor (SMS).
 */
#define ASMC_SMS_INIT1		0xe0
#define ASMC_SMS_INIT2		0xf8
#define ASMC_KEY_SMS		"MOCN"	/* RW; 2 bytes */
#define ASMC_KEY_SMS_X		"MO_X"	/* RO; 2 bytes */
#define ASMC_KEY_SMS_Y		"MO_Y"	/* RO; 2 bytes */
#define ASMC_KEY_SMS_Z		"MO_Z"	/* RO; 2 bytes */
#define ASMC_KEY_SMS_LOW	"MOLT"	/* RW; 2 bytes */
#define ASMC_KEY_SMS_HIGH	"MOHT"	/* RW; 2 bytes */
#define ASMC_KEY_SMS_LOW_INT	"MOLD"	/* RW; 1 byte */
#define ASMC_KEY_SMS_HIGH_INT	"MOHD"	/* RW; 1 byte */
#define ASMC_KEY_SMS_FLAG	"MSDW"	/* RW; 1 byte */
#define ASMC_SMS_INTFF		0x60	/* Free fall Interrupt */
#define ASMC_SMS_INTHA		0x6f	/* High Acceleration Interrupt */
#define ASMC_SMS_INTSH		0x80	/* Shock Interrupt */

/*
 * Keyboard backlight.
 */
#define ASMC_KEY_LIGHTLEFT	"ALV0"	/* RO; 6 bytes */
#define ASMC_KEY_LIGHTRIGHT	"ALV1"	/* RO; 6 bytes */
#define ASMC_KEY_LIGHTVALUE	"LKSB"	/* WO; 2 bytes */

/*
 * Clamshell.
 */
#define ASMC_KEY_CLAMSHELL	"MSLD"	/* RO; 1 byte */

/*
 * Interrupt keys.
 */
#define ASMC_KEY_INTOK		"NTOK"	/* WO; 1 byte */

/*
 * Temperatures.
 *
 * First for MacBook, second for MacBook Pro, third for Intel Mac Mini,
 * fourth the Mac Pro 8-core and finally the MacBook Air.
 *
 */

#define ASMC_UNKN_DESC		"Unknown"

/* maximum array size for temperatures including the last NULL */
#define ASMC_TEMP_MAX		80
#define ASMC_MB_TEMPS		{ "TB0T", "TN0P", "TN1P", "Th0H", "Th1H", \
				  "TM0P", NULL }
#define ASMC_MB_TEMPNAMES	{ "enclosure", "northbridge1", \
				  "northbridge2", "heatsink1", \
				  "heatsink2", "memory", }
#define ASMC_MB_TEMPDESCS	{ "Enclosure Bottomside", \
				  "Northbridge Point 1", \
				  "Northbridge Point 2", "Heatsink 1", \
				  "Heatsink 2", "Memory Bank A", }

#define ASMC_MBP_TEMPS		{ "TB0T", "Th0H", "Th1H", "Tm0P", \
				  "TG0H", "TG0P", "TG0T", NULL }

#define ASMC_MBP_TEMPNAMES	{ "enclosure", "heatsink1", \
				  "heatsink2", "memory", "graphics", \
				  "graphicssink", "unknown", }

#define ASMC_MBP_TEMPDESCS	{ "Enclosure Bottomside", \
				  "Heatsink 1", "Heatsink 2", \
				  "Memory Controller", \
				  "Graphics Chip", "Graphics Heatsink", \
				  ASMC_UNKN_DESC, } 

#define ASMC_MBP4_TEMPS		{ "TB0T", "Th0H", "Th1H", "Th2H", "Tm0P", \
				  "TG0H", "TG0D", "TC0D", "TC0P", "Ts0P", \
				  "TTF0", "TW0P", NULL }

#define ASMC_MBP4_TEMPNAMES	{ "enclosure", "heatsink1", "heatsink2", \
				  "heatsink3", "memory", "graphicssink", \
				  "graphics", "cpu", "cpu2", "unknown1", \
				  "unknown2", "wireless", }

#define ASMC_MBP4_TEMPDESCS	{ "Enclosure Bottomside", \
				  "Main Heatsink 1", "Main Heatsink 2", \
				  "Main Heatsink 3", \
				  "Memory Controller", \
				  "Graphics Chip Heatsink", \
				  "Graphics Chip Diode", \
				  "CPU Temperature Diode", "CPU Point 2", \
				  ASMC_UNKN_DESC, ASMC_UNKN_DESC, \
				  "Wireless Module", } 

#define ASMC_MBP8_TEMPS		{ "TB0T", "TB1T", "TB2T", "TC0C", "TC0D", \
				  "TC0E", "TC0F", "TC0P", "TC1C", "TC2C", \
				  "TC3C", "TC4C", "TCFC", "TCGC", "TCSA", \
				  "TCTD", "TG0D", "TG0P", "THSP", "TM0S", \
				  "TMBS", "TP0P", "TPCD", "TW0P", "Th1H", \
				  "Th2H", "Tm0P", "Ts0P", "Ts0S", NULL } 

#define ASMC_MBP8_TEMPNAMES	{ "enclosure", "TB1T", "TB2T", "TC0C", "TC0D", \
				  "TC0E", "TC0F", "TC0P", "TC1C", "TC2C", \
				  "TC3C", "TC4C", "TCFC", "TCGC", "TCSA", \
				  "TCTD", "graphics", "TG0P", "THSP", "TM0S", \
				  "TMBS", "TP0P", "TPCD", "wireless", "Th1H", \
				  "Th2H", "memory", "Ts0P", "Ts0S" } 

#define ASMC_MBP8_TEMPDESCS	{ "Enclosure Bottomside", "TB1T", "TB2T", "TC0C", "TC0D", \
				  "TC0E", "TC0F", "TC0P", "TC1C", "TC2C", \
				  "TC3C", "TC4C", "TCFC", "TCGC", "TCSA", \
				  "TCTD", "TG0D", "TG0P", "THSP", "TM0S", \
				  "TMBS", "TP0P", "TPCD", "TW0P", "Th1H", \
				  "Th2H", "Tm0P", "Ts0P", "Ts0S" } 

#define ASMC_MBP11_TEMPS	{ "TB0T", "TB1T", "TB2T", "TBXT", "TC0E", \
				  "TC0F", "TC0P", "TC1C", "TC2C", "TC3C", \
				  "TC4C", "TCFC", "TCGC", "TCSA", "TCTD", \
				  "TCXC", "TG0D", "TG0P", "TG1D", "TG1F", \
				  "TG1d", "TH0A", "TH0B", "TH0F", "TH0R", \
				  "TH0V", "TH0a", "TH0b", "TH0c", "TM0P", \
				  "TM0S", "TP0P", "TPCD", "TW0P", "Ta0P", \
				  "TaSP", "Th1H", "Th2H", "Ts0P", "Ts0S", \
				  "Ts1S", NULL } 

#define ASMC_MBP11_TEMPNAMES	{ "TB0T", "TB1T", "TB2T", "TBXT", "TC0E", \
				  "TC0F", "TC0P", "TC1C", "TC2C", "TC3C", \
				  "TC4C", "TCFC", "TCGC", "TCSA", "TCTD", \
				  "TCXC", "TG0D", "TG0P", "TG1D", "TG1F", \
				  "TG1d", "TH0A", "TH0B", "TH0F", "TH0R", \
				  "TH0V", "TH0a", "TH0b", "TH0c", "TM0P", \
				  "TM0S", "TP0P", "TPCD", "TW0P", "Ta0P", \
				  "TaSP", "Th1H", "Th2H", "Ts0P", "Ts0S", \
				  "Ts1S" } 

#define ASMC_MBP11_TEMPDESCS	{ "TB0T", "TB1T", "TB2T", "TBXT", "TC0E", \
				  "TC0F", "TC0P", "TC1C", "TC2C", "TC3C", \
				  "TC4C", "TCFC", "TCGC", "TCSA", "TCTD", \
				  "TCXC", "TG0D", "TG0P", "TG1D", "TG1F", \
				  "TG1d", "TH0A", "TH0B", "TH0F", "TH0R", \
				  "TH0V", "TH0a", "TH0b", "TH0c", "TM0P", \
				  "TM0S", "TP0P", "TPCD", "TW0P", "Ta0P", \
				  "TaSP", "Th1H", "Th2H", "Ts0P", "Ts0S", \
				  "Ts1S" } 

#define ASMC_MM_TEMPS		{ "TN0P", "TN1P", NULL }
#define ASMC_MM_TEMPNAMES	{ "northbridge1", "northbridge2" }
#define ASMC_MM_TEMPDESCS	{ "Northbridge Point 1", \
				  "Northbridge Point 2" }

#define MM_TA0P_NAME		"airflow"
#define MM_TA0P_DESC		"Air Flow Temperature"

#define MM_TC0D_NAME		"cpu0_die"
#define MM_TC0D_DESC		"CPU0 Die Core Temperature"

#define MM_TC0H_NAME		"cpu0_heatsink"
#define MM_TC0H_DESC		"CPU0 Heatsink Temperature"

#define MM_TC0P_NAME		"cpu0_proximity"
#define MM_TC0P_DESC		"CPU0 Proximity Temperature"

#define MM_TH0P_NAME		"hdd_bay"
#define MM_TH0P_DESC		"HDD Bay Temperature"

#define MM_TN0D_NAME		"northbridge_die"
#define MM_TN0D_DESC		"Northbridge Die Core Temperature"

#define MM_TN0P_NAME		"northbridge_proximity"
#define MM_TN0P_DESC		"Northbridge Proximity Temperature"

#define MM_TW0P_NAME		"wireless_module"
#define MM_TW0P_DESC		"Wireless Module Temperature"

#define MM_TO0P_NAME		"optical_drive"
#define MM_TO0P_DESC		"Optical Drive Temperature"

#define MM_TM0P_NAME		"memory"
#define MM_TM0P_DESC		"Memory Bank A Temperature"

#define MM_Tm0P_NAME		"mainboard"
#define MM_Tm0P_DESC		"Mainboard Proximity Temperature"

#define MM_Tp0C_NAME		"power_supply"
#define MM_Tp0C_DESC		"Power Supply Temperature"

#define ASMC_MM31_TEMPS		{ "TC0D", "TC0H", \
				  "TC0P", "TH0P", \
				  "TN0D", "TN0P", \
				  "TW0P", NULL }

#define ASMC_MM31_TEMPNAMES	{ MM_TC0D_NAME, MM_TC0H_NAME, MM_TC0P_NAME, \
				  MM_TH0P_NAME, MM_TN0D_NAME, MM_TN0P_NAME, \
				  MM_TW0P_NAME }

#define ASMC_MM31_TEMPDESCS	{ MM_TC0D_DESC, MM_TC0H_DESC, MM_TC0P_DESC, \
				  MM_TH0P_DESC, MM_TN0D_DESC, MM_TN0P_DESC, \
				  MM_TW0P_DESC }

#define ASMC_MM41_FAN_FUNCS	asmc_mb_sysctl_fanid, asmc_mb_sysctl_fanspeed, \
				  NULL, asmc_mb_sysctl_fanminspeed, \
				  asmc_mb_sysctl_fanmaxspeed, \
				  asmc_mb_sysctl_fantargetspeed, \
				  asmc_mb_sysctl_fanmanual

#define ASMC_MM41_TEMPS		{ "TA0P", "TC0D", "TC0G", "TC0H", "TC0P", \
				  "TC0p", "TCPG", "TH0G", "TH0P", "TH0p", \
				  "TM0G", "TM0P", "TM0p", "TN0D", "TN0G", \
				  "TN0P", "TN0p", "TN1D", "TN1E", "TN1F", \
				  "TN1G", "TN1S", "TNPG", "TO0P", "TO0p", \
				  "TW0P", "Tm0P", "Tp0C", "Tp0c", NULL }

#define ASMC_MM41_TEMPNAMES	{ MM_TA0P_NAME, MM_TC0D_NAME, "TC0G", \
				  MM_TC0H_NAME, MM_TC0P_NAME, "TC0p", "TCPG", \
				  "TH0G", MM_TH0P_NAME, "TH0p", "TM0G", \
				  MM_TM0P_NAME, "TM0p", MM_TN0D_NAME, "TN0G", \
				  MM_TN0P_NAME, "TN0p", "TN1D", "TN1E", \
				  "TN1F", "TN1G", "TN1S", "TNPG", \
				  MM_TO0P_NAME, "TO0p", MM_TW0P_NAME, \
				  MM_Tm0P_NAME, MM_Tp0C_NAME, "Tp0c" }

#define ASMC_MM41_TEMPDESCS	{ MM_TA0P_DESC, MM_TC0D_DESC, ASMC_UNKN_DESC, \
				  MM_TC0H_DESC,MM_TC0P_DESC, ASMC_UNKN_DESC, \
				  ASMC_UNKN_DESC, ASMC_UNKN_DESC, \
				  MM_TH0P_DESC, ASMC_UNKN_DESC, \
				  ASMC_UNKN_DESC, MM_TM0P_DESC, \
				  ASMC_UNKN_DESC, MM_TN0D_DESC, \
				  ASMC_UNKN_DESC, MM_TN0P_DESC, \
				  ASMC_UNKN_DESC, ASMC_UNKN_DESC, \
				  ASMC_UNKN_DESC, ASMC_UNKN_DESC, \
				  ASMC_UNKN_DESC, ASMC_UNKN_DESC, \
				  ASMC_UNKN_DESC, MM_TO0P_DESC, \
				  ASMC_UNKN_DESC, MM_TW0P_DESC, MM_Tm0P_DESC, \
				  MM_Tp0C_DESC, ASMC_UNKN_DESC }

#define ASMC_MP_TEMPS		{ "TA0P", "TCAG", "TCAH", "TCBG", "TCBH", \
				  "TC0C", "TC0D", "TC0P", "TC1C", "TC1D", \
				  "TC2C", "TC2D", "TC3C", "TC3D", "THTG", \
				  "TH0P", "TH1P", "TH2P", "TH3P", "TMAP", \
				  "TMAS", "TMBS", "TM0P", "TM0S", "TM1P", \
				  "TM1S", "TM2P", "TM2S", "TM3S", "TM8P", \
				  "TM8S", "TM9P", "TM9S", "TN0H", "TS0C", \
				  NULL }

#define ASMC_MP_TEMPNAMES	{ "TA0P", "TCAG", "TCAH", "TCBG", "TCBH", \
				  "TC0C", "TC0D", "TC0P", "TC1C", "TC1D", \
				  "TC2C", "TC2D", "TC3C", "TC3D", "THTG", \
				  "TH0P", "TH1P", "TH2P", "TH3P", "TMAP", \
				  "TMAS", "TMBS", "TM0P", "TM0S", "TM1P", \
				  "TM1S", "TM2P", "TM2S", "TM3S", "TM8P", \
				  "TM8S", "TM9P", "TM9S", "TN0H", "TS0C", }

#define ASMC_MP_TEMPDESCS	{ "TA0P", "TCAG", "TCAH", "TCBG", "TCBH", \
				  "TC0C", "TC0D", "TC0P", "TC1C", "TC1D", \
				  "TC2C", "TC2D", "TC3C", "TC3D", "THTG", \
				  "TH0P", "TH1P", "TH2P", "TH3P", "TMAP", \
				  "TMAS", "TMBS", "TM0P", "TM0S", "TM1P", \
				  "TM1S", "TM2P", "TM2S", "TM3S", "TM8P", \
				  "TM8S", "TM9P", "TM9S", "TN0H", "TS0C", }

#define ASMC_MP5_TEMPS		{ "TA0P", "TCAC", "TCAD", "TCAG", "TCAH", \
				  "TCAS", "TCBC", "TCBD", "TCBG", "TCBH", \
				  "TCBS", "TH1F", "TH1P", "TH1V", "TH2F", \
				  "TH2P", "TH2V", "TH3F", "TH3P", "TH3V", \
				  "TH4F", "TH4P", "TH4V", "THPS", "THTG", \
				  "TM1P", "TM2P", "TM2V", "TM3P", "TM3V", \
				  "TM4P", "TM5P", "TM6P", "TM6V", "TM7P", \
				  "TM7V", "TM8P", "TM8V", "TM9V", "TMA1", \
				  "TMA2", "TMA3", "TMA4", "TMB1", "TMB2", \
				  "TMB3", "TMB4", "TMHS", "TMLS", "TMPS", \
				  "TMPV", "TMTG", "TN0D", "TN0H", "TNTG", \
				  "Te1F", "Te1P", "Te1S", "Te2F", "Te2S", \
				  "Te3F", "Te3S", "Te4F", "Te4S", "Te5F", \
				  "Te5S", "TeGG", "TeGP", "TeRG", "TeRP", \
				  "TeRV", "Tp0C", "Tp1C", "TpPS", "TpTG", \
				  NULL }

#define ASMC_MP5_TEMPNAMES	{ "ambient", "TCAC", "TCAD", "TCAG", "TCAH", \
				  "TCAS", "TCBC", "TCBD", "TCBG", "TCBH", \
				  "TCBS", "TH1F", "TH1P", "TH1V", "TH2F", \
				  "TH2P", "TH2V", "TH3F", "TH3P", "TH3V", \
				  "TH4F", "TH4P", "TH4V", "THPS", "THTG", \
				  "TM1P", "TM2P", "TM2V", "TM3P", "TM3V", \
				  "TM4P", "TM5P", "TM6P", "TM6V", "TM7P", \
				  "TM7V", "TM8P", "TM8V", "TM9V", "ram_a1", \
				  "ram_a2", "ram_a3", "ram_a4", "ram_b1", "ram_b2", \
				  "ram_b3", "ram_b4", "TMHS", "TMLS", "TMPS", \
				  "TMPV", "TMTG", "TN0D", "TN0H", "TNTG", \
				  "Te1F", "Te1P", "Te1S", "Te2F", "Te2S", \
				  "Te3F", "Te3S", "Te4F", "Te4S", "Te5F", \
				  "Te5S", "TeGG", "TeGP", "TeRG", "TeRP", \
				  "TeRV", "Tp0C", "Tp1C", "TpPS", "TpTG", }

#define ASMC_MP5_TEMPDESCS	{ "TA0P", "TCAC", "TCAD", "TCAG", "TCAH", \
				  "TCAS", "TCBC", "TCBD", "TCBG", "TCBH", \
				  "TCBS", "TH1F", "TH1P", "TH1V", "TH2F", \
				  "TH2P", "TH2V", "TH3F", "TH3P", "TH3V", \
				  "TH4F", "TH4P", "TH4V", "THPS", "THTG", \
				  "TM1P", "TM2P", "TM2V", "TM3P", "TM3V", \
				  "TM4P", "TM5P", "TM6P", "TM6V", "TM7P", \
				  "TM7V", "TM8P", "TM8V", "TM9V", "TMA1", \
				  "TMA2", "TMA3", "TMA4", "TMB1", "TMB2", \
				  "TMB3", "TMB4", "TMHS", "TMLS", "TMPS", \
				  "TMPV", "TMTG", "TN0D", "TN0H", "TNTG", \
				  "Te1F", "Te1P", "Te1S", "Te2F", "Te2S", \
				  "Te3F", "Te3S", "Te4F", "Te4S", "Te5F", \
				  "Te5S", "TeGG", "TeGP", "TeRG", "TeRP", \
				  "TeRV", "Tp0C", "Tp1C", "TpPS", "TpTG", }

#define	ASMC_MBA_TEMPS		{ "TB0T", NULL }
#define	ASMC_MBA_TEMPNAMES	{ "enclosure" }
#define	ASMC_MBA_TEMPDESCS	{ "Enclosure Bottom" }

#define	ASMC_MBA3_TEMPS		{ "TB0T", "TB1T", "TB2T", \
				  "TC0D", "TC0E", "TC0P", NULL }

#define	ASMC_MBA3_TEMPNAMES	{ "enclosure", "TB1T", "TB2T", \
				  "TC0D", "TC0E", "TC0P" }

#define	ASMC_MBA3_TEMPDESCS	{ "Enclosure Bottom", "TB1T", "TB2T", \
				  "TC0D", "TC0E", "TC0P" }
